import javassist.ClassPool;
import javassist.CtClass;

public class AsposeWords_21_1 {
    public static void main(String[] args) throws Exception {
        ClassPool.getDefault().insertClassPath("aspose-words-21.1-jdk17.jar");
        CtClass clazz = ClassPool.getDefault().getCtClass("com.aspose.words.zzZE0");
        clazz.getDeclaredMethod("zzZ4h").setBody("{return 1;}");
        clazz.getDeclaredMethod("zzZ4g").setBody("{return 1;}");
        clazz.writeFile();
    }
}
